$(document).ready(function() {

	/*Custom select inputs*/
	$('.top__language__select__list').selectmenu();

	$('.request__form select').selectmenu();

	$('.popup__form__wrap select').selectmenu();


	/*Top choose city*/
	$('.trigger__item').click(function() {
		$('.sub-list').toggleClass('active');
	});

	$('.sub__list__item').click(function() {
		var text = $(this).html();
		$('.trigger__item').html(text);
		$('.sub-list').removeClass('active');
	});

	/*Скрываем и показываем меню на экранах меньше 1000px*/
	if ($(window).width() < 1000 ) {

		$('.menu__trigger').click(function() {
			$(this).toggleClass('is-active');
			$('.top__nav__list').slideToggle(300);
		});

		$('.sub-menu-link').click(function(i) {
			i.preventDefault();
			$(this).next('.top__subnav__list').slideToggle();
		});

	} else {
		console.log('something wrong');
	}

	$('.main__slider').slick();

	$('.reviews__slider').slick({
  dots: false,
  infinite: true,
  speed: 600,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

	$('.teacher__slider').slick({
  dots: false,
  infinite: true,
  speed: 600,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

	$('.video__slider').slick({
  dots: false,
  infinite: true,
  speed: 600,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

	/*Кликаем вызывая формочки*/
	$('.underlay').click(function() {
		$('.popup__form__wrap').fadeOut(450);
		$(this).fadeOut(500);
	});

	$('.close-small').click(function() {
		$('.popup__form__wrap').fadeOut(450);
		$('.underlay').fadeOut(500);
	});

	$('.callback--order').click(function(i) {
		i.preventDefault(i);
		$('.underlay').fadeIn(300);
		$('.callback__popup').delay(50).fadeIn(500);
	});

	$('.translate-order, .kids__free__lesson__link').click(function(i) {
		i.preventDefault(i);
		$('.underlay').fadeIn(300);
		$('.translate__popup').delay(50).fadeIn(500);
	});

	$('.free--lesson').click(function(i) {
		i.preventDefault(i);
		$('.underlay').fadeIn(300);
		$('.lesson__popup').delay(50).fadeIn(500);
	});
	
});